# git files

My personal git files.

# Required for I3

 - I3-gaps
 - ibus
 - polybar
 - feh
 - rofi
 - picom

# Required fonts
 - NotoSansMono Nerd Font
 - Caskaydia Cove Nerd Font
 - Jetbrains Mono Nerd Font
 - Material Design Icons
 - Unifont

# Optional Required

 - Oh My Zsh
 - neovim
 - alacritty

# Example
![screenshot](example.png)

